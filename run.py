from backend.preprocessing import *
from backend.metrics import *
from backend.gan import GAN
from backend.tcn import make_TCN

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
#import pandas_datareader as pdr
import yfinance as yf
yf.pdr_override()

from scipy.stats import norm, pearsonr
#from tensorflow.random import normal
import torch
#from google.colab import files


#------
data = yf.download("^GSPC", start="2009-01-01", end="2019-12-31")
df = data['Adj Close']
log_returns = np.log(df/df.shift(1))[1:].to_numpy().reshape(-1, 1)

#---
s1 = StandardScaler()
s2 = StandardScaler()
g1 = Gaussianize()

# receptive field size
rfs = 127

# pre-processing pipeline
log_returns_preprocessed = s2.fit_transform(g1.fit_transform(s1.fit_transform(log_returns)))
log_returns_rolled = rolling_window(log_returns_preprocessed, rfs)


#---
dilations = 2**(np.arange(6))
n_filters = 100

discriminator = make_TCN(dilations, n_filters, 0, False, False, False, [1, rfs, 1], isGen=False)
generator = make_TCN(dilations, n_filters, 0, True, False, False, [1, None, 3])

#---

class verboseGAN(GAN):
    # override the train hook method to print some metrics during training
    def train_hook(self, n_batch):
        if (n_batch + 1) % 500 == 0:
            y = self.generator(self.fixed_noise).detach().numpy().squeeze()
            scores = []
            scores.append(np.linalg.norm(self.acf_real - acf(y.T, 250).mean(axis=1, keepdims=True)))
            scores.append(np.linalg.norm(self.abs_acf_real - acf(y.T ** 2, 250).mean(axis=1, keepdims=True)))
            scores.append(np.linalg.norm(self.le_real - acf(y.T, 250, le=True).mean(axis=1, keepdims=True)))
            print("\nacf: {:.4f}, acf_abs: {:.4f}, le: {:.4f}".format(*scores))


gan = verboseGAN(discriminator, generator, 2 * rfs - 1, lr_d=1e-4, lr_g=3e-5, generator_input_shape=[1, None, 3])

gan.acf_real = acf(log_returns_preprocessed, 250)
gan.abs_acf_real = acf(log_returns_preprocessed ** 2, 250)
gan.le_real = acf(log_returns_preprocessed, 250, le=True)

# noise that stays constant for displaying metrics
gan.fixed_noise = torch.randn([128, 1, 4000 + rfs - 1, 3])


#--
data = np.expand_dims(np.moveaxis(log_returns_rolled, 0,1), 1).astype('float32')
batch_size = 64
n_batches = 3000
additional_d_steps = 0

print('data.shape', data.shape)

# It may appear that I'm stopping while acf_abs is still significantly decreasing,
# but it has the tendency to increase after ~3000 steps.
device = torch.device("cuda:0") if torch.cuda.is_available() else torch.device("cpu")
gan.train(data, batch_size, n_batches, additional_d_steps, device)