import numpy as np
import torch
import torch.nn as nn
from torch.nn.utils import weight_norm


class Chomp1d(nn.Module):
    def __init__(self, chomp_size):
        super(Chomp1d, self).__init__()
        self.chomp_size = chomp_size

    def forward(self, x):
        return x[:, :, :-self.chomp_size].contiguous()


class TemporalBlock(nn.Module):
    def __init__(self, n_inputs, n_outputs, kernel_size, stride, dilation, padding, dropout=0.2):
        super(TemporalBlock, self).__init__()
        print(f'TemporalBlock(n_inputs={n_inputs}, n_outputs={n_outputs}, kernel_size={kernel_size}, stride={stride}, dilation={dilation}, padding={padding})')
        self.conv1 = weight_norm(nn.Conv1d(n_inputs, n_outputs, kernel_size,
                                           stride=stride, padding=padding, dilation=dilation))
        self.chomp1 = Chomp1d(padding)
        self.relu1 = nn.ReLU()
        self.dropout1 = nn.Dropout(dropout)

        self.conv2 = weight_norm(nn.Conv1d(n_outputs, n_outputs, kernel_size,
                                           stride=stride, padding=padding, dilation=dilation))
        self.chomp2 = Chomp1d(padding)
        self.relu2 = nn.ReLU()
        self.dropout2 = nn.Dropout(dropout)
        '''
        self.net = nn.Sequential(self.conv1, self.chomp1, self.relu1, self.dropout1,
                                 self.conv2, self.chomp2, self.relu2, self.dropout2)
        '''
        self.net = nn.Sequential(self.conv1, self.chomp1, self.relu1, self.dropout1)
        self.net2 = nn.Sequential(self.conv2, self.chomp2, self.relu2, self.dropout2)
        self.downsample = nn.Conv1d(n_inputs, n_outputs, 1) if n_inputs != n_outputs else None
        self.relu = nn.ReLU()
        self.init_weights()

    def init_weights(self):
        self.conv1.weight.data.normal_(0, 0.01)
        self.conv2.weight.data.normal_(0, 0.01)
        if self.downsample is not None:
            self.downsample.weight.data.normal_(0, 0.01)

    def forward(self, x):
        print('block in', x.shape)
        out = self.net(x)
        res = x if self.downsample is None else self.downsample(x)
        print('block out1', out.shape, res.shape)
        out = self.net2(out)
        print('block out2', out.shape, res.shape)
        return self.relu(out + res)


class TemporalConvNet(nn.Module):
    def __init__(self, num_inputs, num_channels, kernel_size=2, dropout=0.2):
        super(TemporalConvNet, self).__init__()
        layers = []
        num_levels = len(num_channels)
        for i in range(num_levels):
            dilation_size = 2 ** i
            in_channels = num_inputs if i == 0 else num_channels[i-1]
            out_channels = num_channels[i]
            layers += [TemporalBlock(in_channels, out_channels, kernel_size, stride=1, dilation=dilation_size,
                                     padding=(kernel_size-1) * dilation_size, dropout=dropout)]

        self.network = nn.Sequential(*layers)

    def forward(self, x):
        return self.network(x)


class ReshapeForGenerator(nn.Module):
    def __init__(self, *args):
        super(ReshapeForGenerator, self).__init__()
        self.shape = args

    def forward(self, x):
        return x.reshape(self.shape)


class SwapAxesStep(nn.Module):
    def __init__(self, *args):
        super(SwapAxesStep, self).__init__()
        self.axes = args

    def forward(self, x):
        return torch.swapaxes(x, self.axes)


class PrepareGeneratorFor1dTCN(nn.Module):
    def __init__(self, *args, in_features, out_features):
        super(PrepareGeneratorFor1dTCN, self).__init__()
        self.linear = nn.Linear(in_features=in_features, out_features=out_features * 2)
        self.conv = torch.nn.Conv1d(in_channels=3, out_channels=2, kernel_size=2, padding=1, dilation = out_features + 1)

    def forward(self, x):
        # x.shape (batch_size, n_series = 1, train_input = 253 for example, n_features = 3)
        x = torch.swapaxes(x, 1, 3) # (64, 1, 253, 3) -> (64, 3, 253, 1)
        x = x.squeeze(-1) # (64, 3, 253, 1) -> (64, 3, 253)
        x = self.conv.forward(x)
        #x = torch.swapaxes(x, 1, 2)
        print('PrepareGeneratorFor1dTCN out shape', x.shape)
        return x

        x = x.squeeze(1) # (64, 1, 253, 3) -> (64, 253, 3)
        x = torch.swapaxes(x, 1, 2) # (64, 253, 3) -> (64, 3, 253)
        x = x.reshape(x.shape[0], x.shape[1] * x.shape[2]) # (64, 3, 253) -> (64, 3 * 253)
        x = self.linear(x) # (64, 3 * 253) -> (64, 127 * 2)
        x = x.reshape(-1, x.shape[-1] // 2, 2) # (64, 127 * 2) -> (64, 127, 2)
        print('PrepareGeneratorFor1dTCN out shape', x.shape)
        return x


class PrepareGeneratorAnswerAfter1dTCN(nn.Module):
    def __init__(self, *args, in_features, out_features):
        super(PrepareGeneratorAnswerAfter1dTCN, self).__init__()
        self.conv = weight_norm(
            nn.Conv1d(in_channels=in_features, out_channels=in_features, kernel_size=(1,))
        )
        self.conv2 = weight_norm(
            nn.Conv1d(in_channels=in_features, out_channels=1, kernel_size=(1,))
        )
        self.relu = nn.ReLU(inplace=True)
        self.relu2 = nn.ReLU(inplace=True)

    def forward(self, x):
        x = self.relu(x)
        x = self.conv(x)
        x = self.relu2(x)
        x = self.conv2(x)
        x = x.unsqueeze(3)
        print('PrepareGeneratorAnswerAfter1dTCN out shape', x.shape)
        return x

        # x.shape (batch_size, 1, rfs = 127, 2 ???)
        x = self.conv(x) # (64, 127, 2) -> (64, 127, 1)
        x = x.unsqueeze(1) # (64, 127, 1) -> (64, 1, 127, 1)
        x = self.relu(x)
        x = self.sigmoid(x)
        print('PrepareGeneratorAnswerAfter1dTCN out shape', x.shape)
        return x


class PrepareDiscriminatorFor1dTCN(nn.Module):
    def __init__(self, *args, in_features, out_features):
        super(PrepareDiscriminatorFor1dTCN, self).__init__()
        self.linear = nn.Linear(in_features=in_features, out_features=out_features * 2)

    def forward(self, x):
        # x.shape (batch_size, n_series = 1, sequence_length = rfs = 127, 1)
        x = x.squeeze() # (64, 1, 127, 1) -> (64, 127)
        x = self.linear(x) # (64, 127) -> (64, 127 * 2)
        x = x.reshape(-1, x.shape[-1] // 2, 2) # (64, 127 * 2) -> (64, 127, 2)
        print('PrepareDiscriminatorFor1dTCN out shape', x.shape)
        return x


class PrepareDiscriminatorAnswerAfter1dTCN(nn.Module):
    def __init__(self, *args, in_features, out_features):
        super(PrepareDiscriminatorAnswerAfter1dTCN, self).__init__()
        self.conv = weight_norm(
            nn.Conv1d(in_channels=in_features, out_channels=in_features, kernel_size=(2,))
        )
        self.conv2 = weight_norm(
            nn.Conv1d(in_channels=in_features, out_channels=1, kernel_size=(1,))
        )
        self.relu = nn.ReLU(inplace=True)
        self.relu2 = nn.ReLU(inplace=True)

    def forward(self, x):
        # x.shape (batch_size, 1, 2)
        x = self.relu(x)
        x = self.conv(x)
        x = self.relu2(x)
        x = self.conv2(x)
        x = x.unsqueeze(3)
        print('PrepareDiscriminatorAnswerAfter1dTCN out shape', x.shape)
        return x


# Constructor
def make_TCN(dilations, fixed_filters, moving_filters, use_batchNorm, one_series_output, sigmoid, input_dim,
             block_size=2, halve_output_series=False, isGen = True):
    """Creates a causal temporal convolutional network with skip connections.
       This network uses 2D convolutions in order to model multiple timeseries co-dependency.
    Args:
        dilations (list, tuple): Ordered number of dilations to use for the temporal blocks.
        fixed_filters (int): Number of channels in the hidden layers corresponding fixed over series axis.
        moving_filters (int): Number of channels in the hidden layers moving over series axis.
        use_batchNorm (bool): Whether to use batch normalization in the temporal blocks. Includes batch Renormalization.
        one_series_output (bool): Whether to collapse the dimension of the series axis to 1 using an additional convolution layer.
        sigmoid (bool): Whether to append the sigmoid activation function at the output of the network.
        input_dim (list, tuple): Input dimension of the shape (number of timeseries, timesteps, number of features). Timesteps may be None for variable length timeseries.
        block_size (int): How many convolution layers to use within a temporal block. Defaults to 2.
	halve_output_series (bool): Halve the amount of amount series, used in conditional generator. Defaults to False.
    Returns:
        tensorflow.keras.models.Model: a non-compiled keras model.
    """

    rfs = receptive_field_size(dilations, block_size)
    n_series = input_dim[0]

    if isGen:
        return nn.Sequential(
            PrepareGeneratorFor1dTCN(in_features=100, out_features=127),
            TemporalConvNet(
            num_inputs=2,#(2 * receptive_field_size - 1, 1) будет входить,
            num_channels=[100, 100, 100, 100, 100, 100], #(receptive_field_size = 127) будет выходить
            kernel_size=2,
            dropout=0.2
        ),
            PrepareGeneratorAnswerAfter1dTCN(in_features=100, out_features=1),
        )


    else:
        return nn.Sequential(
            PrepareDiscriminatorFor1dTCN(in_features=127, out_features=127),
        TemporalConvNet(
            num_inputs=127,#rfs,
            num_channels = [100, 100, 100, 100, 100, 100],#[3] * 7,
            kernel_size=2,#block_size,
            dropout=0.2
        ),
            PrepareDiscriminatorAnswerAfter1dTCN(in_features=100, out_features=1)
        )

    input_layer = Input(shape=input_dim)
    cropping = 0
    prev_layer, skip_layer, _ = add_temporal_block(input_layer, None, 1, 1, fixed_filters, moving_filters, n_series,
                                                   rfs, block_size, use_batchNorm, cropping)

    for dilation in dilations:
        prev_layer, skip_layer, cropping = add_temporal_block(prev_layer, skip_layer, 2, dilation, fixed_filters,
                                                              moving_filters, n_series, rfs, block_size, use_batchNorm,
                                                              cropping)

    output_layer = PReLU(shared_axes=[2, 3])(skip_layer)
    output_layer = SpectralNormalization(Conv2D(fixed_filters + moving_filters, kernel_size=1))(output_layer)
    output_layer = PReLU(shared_axes=[2, 3])(output_layer)
    output_layer = SpectralNormalization(Conv2D(1, kernel_size=1))(output_layer)

    if one_series_output:
        output_layer = SpectralNormalization(Conv2D(1, (n_series, 1)))(output_layer)

    if halve_output_series:
        output_layer = Cropping2D(cropping=((n_series // 2, 0), (0, 0)))(output_layer)

    if sigmoid:
        output_layer = Activation('sigmoid')(output_layer)

    return Model(input_layer, output_layer)


def receptive_field_size(dilations, block_size):
    """Non-exhaustive computation of receptive field size.
    Args:
        dilations (list, tuple): Ordered number of dilations of the network.
        block_size (int): Number of convolution layers in each temporal block of the network.
    Returns:
        int: the receptive field size.
    """
    return 1 + block_size * sum(dilations)



#print(f'add_temporal_block(prev_layer={prev_layer}, skip_layer={skip_layer}, kernel_size={kernel_size}, dilation={dilation}, fixed_filters={fixed_filters}, moving_filters={moving_filters}, n_series={n_series}, rfs={rfs}, block_size={block_size}, use_batchNorm={use_batchNorm}, cropping={cropping})')
#print(f'make_TCN(dilations={dilations}, fixed_filters={fixed_filters}, moving_filters={moving_filters}, use_batchNorm={use_batchNorm}, one_series_output={one_series_output}, sigmoid={sigmoid}, input_dim, block_size={block_size}, halve_output_series={halve_output_series}')