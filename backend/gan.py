# from tensorflow.keras.losses import BinaryCrossentropy
# from tensorflow.keras.optimizers import Adam
# from tensorflow.keras.utils import Progbar
# from tensorflow.keras.models import load_model, Model
# from tensorflow.keras.layers import Input, Concatenate
# from tensorflow import convert_to_tensor
from math import floor, ceil

import pickle
#import tensorflow as tf
import torch.nn as nn
import numpy as np
import torch.optim


class GAN:
    """ Generative adverserial network class.

    Training code for a standard DCGAN using the Adam optimizer.
    Code taken in part from: https://github.com/tensorflow/docs/blob/master/site/en/tutorials/generative/dcgan.ipynb
    """    

    def discriminator_loss(self, real_output, fake_output):
        real_loss = self.loss(real_output, torch.ones_like(real_output)) #self.loss(tf.ones_like(real_output), real_output)
        fake_loss = self.loss(fake_output, torch.zeros_like(fake_output)) #self.loss(tf.zeros_like(fake_output), fake_output)
        total_loss = real_loss + fake_loss
        return total_loss

    def generator_loss(self, fake_output):
        return self.loss(fake_output, torch.ones_like(fake_output))
        #return self.loss(tf.ones_like(fake_output), fake_output)

    def __init__(self, discriminator, generator, training_input, lr_d=1e-4, lr_g=3e-4, epsilon=1e-8, beta_1=.0, beta_2=0.9, from_logits=True, \
                 generator_input_shape=None):
        """Create a GAN instance

        Args:
            discriminator (tensorflow.keras.models.Model): Discriminator model.
            generator (tensorflow.keras.models.Model): Generator model.
            training_input (int): input size of temporal axis of noise samples.
            lr_d (float, optional): Learning rate of discriminator. Defaults to 1e-4.
            lr_g (float, optional): Learning rate of generator. Defaults to 3e-4.
            epsilon (float, optional): Epsilon paramater of Adam. Defaults to 1e-8.
            beta_1 (float, optional): Beta1 parameter of Adam. Defaults to 0.
            beta_2 (float, optional): Beta2 parameter of Adam. Defaults to 0.9.
            from_logits (bool, optional): Output range of discriminator, logits imply output on the entire reals. Defaults to True.
        """
        self.discriminator = discriminator
        self.generator = generator
        #self.noise_shape = [self.generator.input_shape[1], training_input, self.generator.input_shape[-1]]
        #self.noise_shape = [generator_input_shape[0], training_input, generator_input_shape[-1]]
        self.noise_shape = [generator_input_shape[0], training_input, generator_input_shape[-1]]

        self.loss = nn.BCELoss()
        #BinaryCrossentropy(from_logits=from_logits)

        self.generator_optimizer = torch.optim.Adam(lr = lr_g, eps = epsilon, betas=(beta_1, beta_2), params=generator.parameters())
        #Adam(lr_g, epsilon=epsilon, beta_1=beta_1, beta_2=beta_2)
        self.discriminator_optimizer = torch.optim.Adam(lr = lr_g, eps = epsilon, betas=(beta_1, beta_2), params=discriminator.parameters())
        #Adam(lr_d, epsilon=epsilon, beta_1=beta_1, beta_2=beta_2)

    def train(self, data, batch_size, n_batches, additional_d_steps, device):
        """training function of a GAN instance.
        Args:
            data (4d array): Training data in the following shape: (samples, n_series, timesteps, 1).
            batch_size (int): Batch size used during training.
            n_batches (int): Number of update steps taken.
            additional_d_steps (int): Number of extra discriminator training steps during each update.
        """
        progress = Progbar(n_batches)

        for n_batch in range(n_batches):
            print(f'n_batch = {n_batch}')
            # sample uniformly
            batch_idx = np.random.choice(np.arange(data.shape[0]), size=batch_size, replace=(batch_size > data.shape[0]))
            batch = data[batch_idx]
            batch = torch.from_numpy(batch)
            batch.to(device)

            self.train_step(batch, batch_size, additional_d_steps)

            self.train_hook(n_batch)

            progress.update(n_batch + 1)

    #@tf.function
    def train_step(self, data, batch_size, additional_d_steps):

        self.discriminator.train()
        self.generator.eval()

        for _ in range(additional_d_steps + 1):
            print('*self.noise_shape', *self.noise_shape)
            noise = torch.randn([batch_size, *self.noise_shape])
            #noise = tf.random.normal([batch_size, *self.noise_shape])
            print('noise.shape', noise.shape)
            generated_data = self.generator(noise)
            #generated_data = self.generator(noise, training=False)
            print('generated_data.shape', generated_data.shape)

            '''
            with tf.GradientTape() as disc_tape:
                real_output = self.discriminator(data, training=True)
                fake_output = self.discriminator(generated_data, training=True)
                disc_loss = self.discriminator_loss(real_output, fake_output)
            
            gradients_of_discriminator = disc_tape.gradient(disc_loss, self.discriminator.trainable_variables)
            self.discriminator_optimizer.apply_gradients(zip(gradients_of_discriminator, self.discriminator.trainable_variables))
            '''
            self.discriminator.zero_grad()

            real_output = self.discriminator(data)
            fake_output = self.discriminator(generated_data)
            print('discriminator out shape = ', fake_output.shape)
            print(f'discriminator min = {real_output.min()}, {fake_output.min()}; max = {real_output.max()}, {fake_output.max()}')

            disc_loss = self.discriminator_loss(real_output, fake_output)

            disc_loss.backward()
            self.discriminator_optimizer.step()

        self.generator.zero_grad()

        #noise = tf.random.normal([batch_size, *self.noise_shape])
        noise = torch.randn([batch_size, *self.noise_shape])

        '''
        with tf.GradientTape() as gen_tape:
            generated_data = self.generator(noise, training=True)
            fake_output = self.discriminator(generated_data, training=False)
            gen_loss = self.generator_loss(fake_output)
            gradients_of_generator = gen_tape.gradient(gen_loss, self.generator.trainable_variables)
            self.generator_optimizer.apply_gradients(zip(gradients_of_generator, self.generator.trainable_variables))
        '''
        self.generator.train()
        generated_data = self.generator(noise)
        self.discriminator.eval()
        fake_output = self.discriminator(generated_data)
        gen_loss = self.generator_loss(fake_output)

        gen_loss.backward()
        self.generator_optimizer.step()


    def train_hook(self, n_batch):
        """Override this method to insert behaviour at every training update.
           Acces to the instance namespace is provided.
        """        
        pass

class Progbar:

    def __init__(self, n_batches):
        pass

    def update(self, current, values=None, finalize=None):
        pass